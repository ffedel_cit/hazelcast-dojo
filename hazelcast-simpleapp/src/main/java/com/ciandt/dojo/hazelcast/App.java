package com.ciandt.dojo.hazelcast;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{

    private static final String[] names = {"Joe", "Ali", "Avi", "Tom", "Mary", "Jane", "Peter", "John", "Mark", "Erik"};

    public static void main(String[] args) {

        HazelcastInstance instance = Hazelcast.newHazelcastInstance();
        IMap<Integer, String> mapCustomers = instance.getMap("customers");

        int mapSize = mapCustomers.size();

        Random rn = new Random();

        System.out.println("Initial Map Size:" + mapSize);

        if(mapSize>0){
            System.out.println("Updating 1");
            mapCustomers.put(1, names[rn.nextInt(10)]);
        }

        for(int i = mapSize+1;i<=mapSize+3;i++){
            mapCustomers.put(i, names[rn.nextInt(10)]);
        }

        System.out.println("Map Size:" + mapCustomers.size());
        mapCustomers.forEach((k,v)->System.out.println("Item : " + k + " Name : " + v));

        IQueue<String> queueCustomers = instance.getQueue("customers");
        queueCustomers.offer("Tom");
        queueCustomers.offer("Mary");
        queueCustomers.offer("Jane");
        System.out.println("First customer: " + queueCustomers.poll());
        System.out.println("Second customer: "+ queueCustomers.peek());
        System.out.println("Queue size: " + queueCustomers.size());
    }

}
