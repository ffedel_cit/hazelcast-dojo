package com.ciandt.dojo.hazelcast;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;

/**
 * Created by fedel on 8/3/16.
 */
public class HazelcastAddListenerDouble implements EntryAddedListener {

    private Integer total = 0;

    @Override
    public void entryAdded(EntryEvent entryEvent) {
        int value = (Integer) entryEvent.getValue();
        total += value*2;
    }

    public Integer getTotal(){
        return total;
    }
}
