package com.ciandt.dojo.hazelcast;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
public class AppTest{


    @After
    public void afterTest(){
        Hazelcast.shutdownAll();
    }


    //challenge 1
    @Test
    public void testTwoMemberMapSizes() {
        // start the first member
        HazelcastInstance h1 = Hazelcast.newHazelcastInstance();
        // get the map and put 1000 entries
        Map map1 = h1.getMap("testmap");
        for (int i = 0; i < 1000; i++) {
            map1.put(i, "value" + i);
        }
        // check the map size
        Assert.assertEquals(1000, map1.size());
        // start the second member
        HazelcastInstance h2 = Hazelcast.newHazelcastInstance();
        // get the same map from the second member
        Map map2 = h2.getMap("testmap");

        // check the size of map1 again
        Assert.assertEquals(1000, map1.size());
        h1.shutdown();
        // check the size of map2
        Assert.assertEquals(1000, map2.size());


    }

    //challenge 2
    @Test
    public void testTryToUpdateInLockedKeyMap() {
        // start the first member
        HazelcastInstance h1 = Hazelcast.newHazelcastInstance();

        IMap map1 = h1.getMap("testmap");

        map1.put(1, "value");

        HazelcastInstance h2 = Hazelcast.newHazelcastInstance();
        // get the same map from the second member
        IMap map2 = h2.getMap("testmap");

        map1.lock(1);

        try {
            map2.tryPut(1, "anothervalue", 2, TimeUnit.SECONDS);
        }finally {
            map1.unlock(1);
            Assert.assertEquals("value", map1.get(1));
            map2.tryPut(1, "anothervalue", 2, TimeUnit.SECONDS);
            Assert.assertEquals("anothervalue", map1.get(1));
        }

    }

    //challenge 3
    @Test
    public void testAddEventListener() throws InterruptedException {
        // start the first member
        HazelcastInstance h1 = Hazelcast.newHazelcastInstance();
        IMap map1 = h1.getMap("testmap");

        HazelcastAddListener simpleListener = new HazelcastAddListener();
        HazelcastInstance h2 = HazelcastClient.newHazelcastClient();
        IMap map2 = h2.getMap("testmap");


        HazelcastAddListenerDouble doubleListener = new HazelcastAddListenerDouble();
        HazelcastInstance h3 = HazelcastClient.newHazelcastClient();
        IMap map3 = h3.getMap("testmap");

        map2.addEntryListener(simpleListener, true);
        map3.addEntryListener(doubleListener, true);

        map1.put("1", 10);
        map1.put("2", 20);

        map2.put("1", 5);

        Thread.sleep(1000L);

        Assert.assertEquals(10L, map1.get("1"));
        Assert.assertEquals(30L, simpleListener.getTotal().longValue());
        Assert.assertEquals(60L, doubleListener.getTotal().longValue());

    }

    //challenge 4
    @Test
    public void testAddEventListenerList() throws InterruptedException {
        // start the first member
        HazelcastInstance h1 = Hazelcast.newHazelcastInstance();
        IMap map1 = h1.getMap("testmap");

        HazelcastAddListener simpleListener = new HazelcastAddListener();
        HazelcastInstance h2 = HazelcastClient.newHazelcastClient();
        IMap map2 = h2.getMap("testmap");


        HazelcastAddListenerDouble doubleListener = new HazelcastAddListenerDouble();
        HazelcastInstance h3 = HazelcastClient.newHazelcastClient();
        IMap map3 = h3.getMap("testmap");

        map2.addEntryListener(simpleListener, true);
        map3.addEntryListener(doubleListener, true);

        map1.put("1", 10);
        map1.put("2", 20);

        map2.put("1", 5);

        Thread.sleep(1000L);

        Assert.assertEquals(10L, map1.get("1"));
        Assert.assertEquals(30L, simpleListener.getTotal().longValue());
        Assert.assertEquals(60L, doubleListener.getTotal().longValue());

    }


}
